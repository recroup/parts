# Parts

Parts is an ActiveRecord migrations and model helpers for creating and managing PostgreSQL 11+ declarative partitions!. This project is heavily
influnced from pg_party gem.

Features:

- Migration methods for partition specific database operations
- Model methods for querying partitioned data
- Model methods for creating adhoc partitions

Limitations:

- Partition tables are not represented correctly in db/schema.rb � please use the :sql schema format
- Complex partition keys are not supported yet.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'parts'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install parts

## Usage

### Migration Examples

Create range partition on `created_at` with two child partitions:

```ruby
class CreateSomeRangeRecord < ActiveRecord::Migration[5.0]
  def up
    current_date = Date.current

    create_range_parent :some_range_records, :created_at do |t|
      t.text :some_value
      t.timestamps
    end

    add_index :some_range_records, :some_value

    create_range_child_of :some_range_records, 
        "some_range_records_#{current_date.strftime('y%Ym%mw%V')}",
        start_range: current_date.beginning_of_week,
        end_range: current_date.next_week.beginning_of_week

    next_date = current_date.next_week
    
    create_range_child_of :some_range_records,
        "some_range_records_#{next_date.strftime('y%Ym%mw%V')}",
        start_range: next_date.beginning_of_week,
        end_range: next_date.next_week.beginning_of_week
  end
end
```

Create list partition on `status` with two child partitions, second child partition `some_list_records_inactive` is further 
range partitioned on `created_at`. 

```ruby
class CreateSomeListRecord < ActiveRecord::Migration[5.0]
  def up
    create_list_parent :some_list_records, :status do |t|
      t.text :some_value
      t.string :status
      
      t.timestamps
    end

    create_list_child_of :some_list_records, 
        :some_list_records_active,
        :active
        
    create_sub_parent :some_list_records_inactive, 
        parent_table_name: :some_list_records, 
        parent_type: :list,
        parent_values: :inactive,
        type: :range,
        partition_key: :created_at
        
    current_date = Date.current
    
    create_range_child_of :some_list_records_inactive, 
        "some_list_records_inactive_#{current_date.strftime('y%Ym%mw%V')}",
        start_range: current_date.beginning_of_week,
        end_range: current_date.next_week.beginning_of_week

    next_date = current_date.next_week
    
    create_range_child_of :some_list_records_inactive,
        "some_list_records_inactive_#{next_date.strftime('y%Ym%mw%V')}",
        start_range: next_date.beginning_of_week,
        end_range: next_date.next_week.beginning_of_week
  end
end
```

Create hash partition on `id` with 10 child partitions:

```ruby
class CreateSomeRangeRecord < ActiveRecord::Migration[5.0]
  def up
    create_hash_parent :some_huge_table, :id do |t|
        t.text :some_value
        t.timestamps
    end

    add_index :some_range_records, :some_value
    
    # Creates 10 child hash table
    create_hash_children_of :some_huge_table, 10
  end
end
```

Attach an existing table to a range partition:

```ruby
class AttachRangePartition < ActiveRecord::Migration[5.0]
  def up
    current_date = Date.current

    attach_range_child_to \
      :some_existing_range_parent_partition_table,
      :some_range_records,
      start_range: current_date,
      end_range: current_date + 1.day
  end
end
```

Attach an existing table to a list partition:

```ruby
class AttachListPartition < ActiveRecord::Migration[5.0]
  def up
    attach_list_child_to \
      :some_existing_list_parent_partition_table,
      :some_list_records,
      values: 'active'
  end
end
```

Detach a child table from any partition:

```ruby
class DetachPartition < ActiveRecord::Migration[5.0]
  def up
    detach_child_from :parent_table, :child_table
  end
end
```

### Model Examples

Extend your model to support partitioning methods within the model:

```ruby
class SomeRangeRecord < ApplicationRecord
    extend Parts::ModelMethods
end
```

Create child partition from range partition model:

```ruby
current_date = Date.current

SomeRangeRecord.create_range_child("some_range_record_xyz", start_range: current_date + 1.day, end_range: current_date + 2.days)
```

Create child partition from list partition model:

```ruby
SomeListRecord.create_list_child("some_list_record_active", :active)
```

List currently attached partitions:

```ruby
SomeRangeRecord.partitions

SomeListRecord.partitions
```

Retrieve ActiveRecord model class scoped to a child partition:

```ruby
SomeRangeRecord.in_partition(:some_range_records_partition_name)

SomeListRecord.in_partition(:some_list_records_partition_name)

#check for partitions within sub-partitions

SomeListRecord.in_partition(:some_list_sub_parent_partition).partitions
```

Check if any model is partitioned:

```ruby
SomeNonPartitionedTable.partitioned?
# => false

SomePartitionedTable.partitioned?
# => true
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/parts. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Parts project�s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/parts/blob/master/CODE_OF_CONDUCT.md).
