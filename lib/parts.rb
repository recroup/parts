# frozen_string_literal: true

require 'parts/version'
require 'active_support/lazy_load_hooks'

module Parts; end

ActiveSupport.on_load(:active_record) do
  require 'active_record/connection_adapters/postgresql_adapter'
  require 'parts/partition_adapter'
  require 'parts/model_methods'
  require 'parts/generic_model_methods'

  extend Parts::GenericModelMethods

  ActiveRecord::ConnectionAdapters::PostgreSQLAdapter.include(
    Parts::PartitionAdapter
  )
end
