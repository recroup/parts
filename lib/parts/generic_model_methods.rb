# frozen_string_literal: true

module Parts
  module GenericModelMethods
    def partitioned?
      connection.select_values("SELECT 1 FROM pg_partitioned_table WHERE partrelid = #{connection.quote(table_name)}::regclass LIMIT 1;").any?
    end
  end
end
