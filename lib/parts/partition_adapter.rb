# frozen_string_literal: true

module Parts
  module PartitionAdapter
    def create_range_parent(table_name, partition_key, **options, &blk)
      create_parent(table_name, :range, partition_key, options, &blk)
    end

    def create_list_parent(table_name, partition_key, **options, &blk)
      create_parent(table_name, :list, partition_key, options, &blk)
    end

    def create_hash_parent(table_name, partition_key, **options, &blk)
      create_parent(table_name, :hash, partition_key, options, &blk)
    end

    def create_range_child_of(parent_table_name, table_name, start_range:, end_range:, **options)
      create_child_of(parent_table_name, table_name, range_constraint_clause(start_range, end_range), **options)
    end

    def create_list_child_of(parent_table_name, table_name, values, **options)
      create_child_of(parent_table_name, table_name, list_constraint_clause(values), **options)
    end

    def create_hash_child_of(parent_table_name, table_name, modulus:, remainder:, **options)
      create_child_of(parent_table_name, table_name, hash_constraint_clause(modulus, remainder), **options)
    end

    def create_hash_children_of(parent_table_name, total, **options)
      raise "'total' must be an Integer" unless total.is_a?(Integer)

      total.times do |t|
        create_hash_child_of(parent_table_name, "#{parent_table_name}_#{t}", modulus: total, remainder: t, **options)
      end
    end

    def detach_child_from(parent_table_name, child_table_name, lock_timeout: 5)
      execute <<~SQL
        SET LOCAL lock_timeout TO '#{lock_timeout}s';
        ALTER TABLE #{quote_table_name(parent_table_name)}
        DETACH PARTITION #{quote_table_name(child_table_name)};
      SQL
    end

    def attach_list_child_to(parent_table_name, child_table_name, values, **options)
      attach_child_to(parent_table_name, child_table_name, list_constraint_clause(values), **options)
    end

    def attach_range_child_to(parent_table_name, child_table_name, start_range:, end_range:)
      attach_child_to(parent_table_name, child_table_name, range_constraint_clause(start_range, end_range), **options)
    end

    def attach_hash_child_to(parent_table_name, child_table_name, modulus:, remainder:, **options)
      attach_child_to(parent_table_name, child_table_name, hash_constraint_clause(modulus, remainder), **options)
    end

    def attach_child_to(parent_table_name, child_table_name, constraint_clause, lock_timeout: 5)
      execute <<~SQL
        SET LOCAL lock_timeout TO '#{lock_timeout}s';
        ALTER TABLE #{quote_table_name(parent_table_name)}
        ATTACH PARTITION #{quote_table_name(child_table_name)}
        FOR VALUES #{constraint_clause};
      SQL
    end

    def create_sub_parent(table_name, parent_table_name:, parent_type:, parent_values:, type:, partition_key:, lock_timeout: 5) # rubocop:disable Metrics/ParameterLists

      constraint_clause = case parent_type
      when :list
        list_constraint_clause(parent_values)
      when :range
        range_constraint_clause(parent_values[0], parent_values[1])
      when :hash
        hash_constraint_clause(parent_values[0], parent_values[1])
      end

      execute <<~SQL
        SET LOCAL lock_timeout TO '#{lock_timeout}s';
        CREATE TABLE #{table_name} PARTITION OF #{parent_table_name} FOR VALUES #{constraint_clause} PARTITION BY #{type.to_s.upcase} (#{partition_key});
      SQL
    end

    private

    def create_parent(table_name, type, partition_key, options)
      options[:options] = "PARTITION BY #{type.to_s.upcase} (#{quote_partition_key(partition_key)})"
      id               = options.fetch(:id, :bigserial)
      primary_key      = options.fetch(:primary_key) { calculate_primary_key(table_name) } if id != false
      options[:id] = false

      validate_primary_key(primary_key)

      create_table(table_name, **options) do |t|
        t.column(primary_key, id, null: false) if id != false

        yield(t) if block_given?
      end
    end

    def create_child_of(parent_table_name, table_name, constraint_clause, lock_timeout: 5)
      execute <<~SQL
        SET LOCAL lock_timeout TO '#{lock_timeout}s';
        CREATE TABLE #{table_name} PARTITION OF #{parent_table_name} FOR VALUES #{constraint_clause};
      SQL
    end

    def calculate_primary_key(table_name)
      ActiveRecord::Base.get_primary_key(table_name.to_s.singularize).to_sym
    end

    def validate_primary_key(key)
      raise ArgumentError, 'composite primary key not supported' if key.is_a?(Array)
    end

    def quote_partition_key(key)
      if key.is_a?(Proc)
        key.call.to_s
      else
        quote_column_name(key)
      end
    end

    def quote_collection(values)
      Array.wrap(values).map(&method(:quote)).join(',')
    end

    def range_constraint_clause(start_range, end_range)
      "FROM (#{quote_collection(start_range)}) TO (#{quote_collection(end_range)})"
    end

    def list_constraint_clause(values)
      "IN (#{quote_collection(values)})"
    end

    def hash_constraint_clause(modulus, remainder)
      "WITH (MODULUS #{modulus}, REMAINDER #{remainder})"
    end
  end
end
