# frozen_string_literal: true

module Parts
  module ModelMethods
    def partitions
      connection.select_values <<~SQL
        SELECT inhrelid::regclass::text FROM pg_inherits WHERE inhparent = #{connection.quote(table_name)}::regclass;
      SQL
    end

    def in_partition(child_table_name)
      Class.new(self) do
        self.table_name = child_table_name

        def self.name
          superclass.name
        end

        def self.allocate
          superclass.allocate
        end

        def self.new(*args, &blk)
          superclass.new(*args, &blk)
        end
      end
    end

    def create_range_child(child_table_name, start_range:, end_range:)
      transaction { connection.create_range_child_of(table_name, child_table_name, start_range: start_range, end_range: end_range) }
    end

    def create_list_child(child_table_name, values)
      transaction { connection.create_list_child_of(table_name, child_table_name, values) }
    end

    def drop_child(child_table_name)
      transaction do
        connection.detach_child_from(table_name, child_table_name) if partitions.include?(child_table_name)
        connection.execute <<-SQL
          DROP TABLE IF EXISTS #{child_table_name};
        SQL
      end
    end
  end
end
